package DatabaseConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseCon {
    private static boolean isConnected = false;
    private static Statement statement;

    private static DatabaseCon instance = new DatabaseCon();

    public static DatabaseCon getInstance() {
        return instance;
    }

    private DatabaseCon() {

        if(isConnected) return;
        else
            try {
                Class.forName("com.mysql.jdbc.Driver");
              Connection connection = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/food?useSSL=false", "root", "Kisame21");
               // Connection connection = DriverManager.getConnection("jdbc:mysql://ipwasteydb.mysql.database.azure.com:3306/ipwasteydb?useSSL=true&requireSSL=false", "stefania@ipwasteydb", "Steff1611226");
                // Connection connection = DriverManager.getConnection("jdbc:mysql://ipwasteydb.mysql.database.azure.com:3306/{your_database}?useSSL=true&requireSSL=false", "stefania", "Steff1611226");

                isConnected = true;
                statement = connection.createStatement();
            } catch (SQLException | ClassNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

    }

    public static  Statement getStatement(){
        return statement;
    }
}
