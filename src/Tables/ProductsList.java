package Tables;

public class ProductsList {

    int id;
    String user_id;
    int product_id;
    int amount;
    int checked;

    public ProductsList(int id, String user_id, int product_id, int amount, int checked) {
        this.id = id;
        this.user_id = user_id;
        this.product_id = product_id;
        this.amount = amount;
        this.checked = checked;
    }

    public ProductsList(String user_id, int product_id, int amount, int checked) {
        this.user_id = user_id;
        this.product_id = product_id;
        this.amount = amount;
        this.checked = checked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getChecked() {
        return checked;
    }

    public void setChecked(int checked) {
        this.checked = checked;
    }
}
