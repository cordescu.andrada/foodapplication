package Tables;

import Services.ProductService;

public class Product extends ProductService{

    int id;
    String name;
    int validity;


    public Product(int id, String name, int validity) {
        this.id = id;
        this.name = name;
        this.validity = validity;
    }

    public Product(String name, int validity) {
        this.name = name;
        this.validity = validity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValidity() {
        return validity;
    }

    public void setValidity(int validity) {
        this.validity = validity;
    }
}
