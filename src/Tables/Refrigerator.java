package Tables;

public class Refrigerator {

    int id;
    String user_id;

    public Refrigerator(int id, String user_id) {
        this.id = id;
        this.user_id = user_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String  user_id) {
        this.user_id = user_id;
    }
}
