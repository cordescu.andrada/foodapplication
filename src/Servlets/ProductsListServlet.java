package Servlets;

import Services.ProductService;
import Services.ProductsListService;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

public class ProductsListServlet extends HttpServlet{

    private String message;

    //        public void init() throws ServletException {
//        //   Do required initialization
//        if (ProductService.getCategoriesString() != null){
//            message = ProductService.getCategoriesString();
//        }
//        else
//            message = "No item in DB\n";
//    }
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json");

        //actual logic

        JSONObject json;
        String userId = request.getParameter("userId");

        if(userId != null && !userId.isEmpty()){
            json = ProductsListService.getJson(ProductsListService.selectById(userId));
        }
        else
            json = ProductsListService.getJson(ProductsListService.slectAll());

        String output = json.toString();
        PrintWriter writer = response.getWriter();
        writer.write(output);
        writer.close();
    }

    @Override
    public void doPost (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json");


        BufferedReader bf = request.getReader();

        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = bf.readLine()) != null) {
            sb.append(line);
            System.out.println(line);
        }
        try {
            JSONObject json = new JSONObject(sb.toString());

            int id = json.getInt("id");
            String userId = json.getString("userId");
            int productId = json.getInt("productId");
            int amount = json.getInt("amount");
            int chcked = json.getInt("checked");


            ProductsListService.insertProduct(id, userId, productId, amount, chcked);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        // else     response.setStatus(404);

        JSONObject jsonObject;

        jsonObject = ProductsListService.getJson(ProductsListService.slectAll());

        String output = jsonObject.toString();
        PrintWriter writer = response.getWriter();
        writer.write(output);
        writer.close();

    }

    @Override
    public void doDelete(HttpServletRequest request,
                         HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json");

        String id = request.getParameter("id");


        ProductsListService.deleteById(Integer.parseInt(id));

        // doGet(request,response);
    }

    @Override
    public void doPut(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {


        String id = request.getParameter("id");

        BufferedReader bf = request.getReader();

        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = bf.readLine()) != null) {
            sb.append(line);
            System.out.println(line);
        }
        try {
            JSONObject json = new JSONObject(sb.toString());

            String userId = json.getString("userId");
            int productId = json.getInt("productId");
            int amount = json.getInt("amount");
            int chcked = json.getInt("checked");



            ProductsListService.updateProduct(Integer.parseInt(id), userId, productId, amount, chcked);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
    public void destroy() {
        // do nothing.
    }
}
