//package Servlets;
//
//import Services.ServiceTest;
//import org.json.JSONObject;
//
//import java.io.*;
//import javax.servlet.*;
//import javax.servlet.http.*;
//
//// Extend HttpServlet class
//public class TestServlet extends HttpServlet {
//
//    private String message;
//
//    public void init() throws ServletException {
//      //   Do required initialization
//
//        if (ServiceTest.getShoppingListString() != null)
//            message = ServiceTest.getShoppingListString();
//        else
//            message = "No item in DB\n";
//    }
//
//    public void doGet(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//
//        JSONObject json = ServiceTest.getShoppingListJsonObject();
//
//        //application type
//        response.setContentType("application/json");
//
//        //actual logic
//        String output = json.toString();
//        PrintWriter writer = response.getWriter();
//        writer.write(output);
//        writer.close();
//    }
//
//    public void destroy() {
//        // do nothing.
//    }
//}