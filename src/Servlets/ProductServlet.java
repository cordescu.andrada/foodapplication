package Servlets;

import Services.ProductService;
import Tables.Product;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jdk.nashorn.internal.parser.JSONParser;
import org.json.JSONException;
import org.json.JSONObject;
import sun.net.www.protocol.http.HttpURLConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.Service;
import java.io.*;
import java.net.URL;


public class ProductServlet extends HttpServlet{
    private String message;

//        public void init() throws ServletException {
//        //   Do required initialization
//        if (ProductService.getCategoriesString() != null){
//            message = ProductService.getCategoriesString();
//        }
//        else
//            message = "No item in DB\n";
//    }
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //  URL urlForGetRequest = new URL("http://localhost:8080/products/1");
//        HttpURLConnection connection = (HttpURLConnection) urlForGetRequest.openConnection();
//        connection.setRequestMethod("GET");
        //application type
        response.setContentType("application/json");

        //actual logic

        JSONObject json;
        String id = request.getParameter("id");

        if(id != null && !id.isEmpty()){
            json =ProductService.getJson(ProductService.selectById(Integer.parseInt(id)));
        }
        else
            json = ProductService.getJson(ProductService.slectAll());

        String output = json.toString();
        PrintWriter writer = response.getWriter();
        writer.write(output);
        writer.close();
    }

    @Override
    public void doPost (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json");


        BufferedReader bf = request.getReader();

        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = bf.readLine()) != null) {
            sb.append(line);
            System.out.println(line);
        }
        try {
            JSONObject json = new JSONObject(sb.toString());

            int id = json.getInt("id");
            String name = json.getString("name");
            int validity = json.getInt("validity");

            ProductService.insertProduct(id,name,validity);

        } catch (JSONException e) {
            e.printStackTrace();
        }

       // else     response.setStatus(404);

        JSONObject jsonObject;

            jsonObject = ProductService.getJson(ProductService.slectAll());

        String output = jsonObject.toString();
        PrintWriter writer = response.getWriter();
        writer.write(output);
        writer.close();

    }

    @Override
    public void doDelete(HttpServletRequest request,
                         HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json");

        String id = request.getParameter("id");


        ProductService.deleteById(Integer.parseInt(id));

       // doGet(request,response);
    }

    @Override
    public void doPut(HttpServletRequest request,
    HttpServletResponse response)
            throws ServletException, IOException {

    }
    public void destroy() {
        // do nothing.
    }
}
