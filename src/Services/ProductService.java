package Services;

import DatabaseConnection.DatabaseCon;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class ProductService {

    private static Statement statement = DatabaseCon.getInstance().getStatement();

    public static boolean getProductById(int id){
        try {
            ResultSet resultSet = statement.executeQuery("SELECT *  from produs where id ='"+id+"'");
            if(resultSet != null )
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void insertProduct(int id, String productName, int validity){
        if(!getProductById(id)){
            System.out.println("ID already exist in the shopping lists");
        }
        else {
            try {
                statement.executeUpdate("INSERT INTO produs(id, nume, valabilitate_estimata )" +
                        " VALUE ('" + id + "','" + productName +  "','" + validity + "')");
                System.out.println("Shopping list inserted from database");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    public static String getCategoriesString()
    {
        String shList = "Result Set: ";
        try {
            ResultSet rs = statement.executeQuery("select * from produs");
            while(rs.next()) {
                shList += rs.getString(1) + " " + rs.getString(2)+" "+rs.getString(3);
            }
            return shList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return shList;

    }

//    public static JSONObject getProductsJsonObject()
//    {
//        JSONObject jsonObject = new JSONObject();
//        try {
//            ResultSet rs = statement.executeQuery("select * from produs");
//
//            while(rs.next()) {
//                //creating a map with all elements in the database
//                Map<String,Object> map = new HashMap();
//
//                map.put("id", Integer.parseInt(rs.getString(1)));
//                map.put("name", rs.getString(2));
//                map.put("validity", Integer.parseInt(rs.getString(3)));
//                //creating the json object
//                jsonObject.append("data", new JSONObject(map));
//            }
//            return jsonObject;
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//
//        return jsonObject;
//    }

    public static ResultSet selectById(int id){
        try {
            ResultSet resultSet = statement.executeQuery("SELECT *  from produs where id ='"+id+"'");
            if(resultSet != null )
                return resultSet;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ResultSet slectAll(){
        try {
            ResultSet resultSet = statement.executeQuery("SELECT *  from produs ");
            if(resultSet != null )
                return resultSet;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static void deleteById(int id){
        try {
            statement.executeUpdate("delete from produs where id ='"+id+"'");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static JSONObject getJson(ResultSet rs){
        JSONObject jsonObject = new JSONObject();
        try {
            while(rs.next()) {
                //creating a map with all elements in the database
                Map<String,Object> map = new HashMap();

                map.put("id", rs.getInt(1));
                map.put("name", rs.getString(2));
                map.put("validity", rs.getInt(3));
                //creating the json object
                jsonObject.append("data", new JSONObject(map));
            }
            return jsonObject;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jsonObject;

    }


}
