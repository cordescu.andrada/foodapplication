package Services;

import DatabaseConnection.DatabaseCon;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class ProductsListService {

    private static Statement statement = DatabaseCon.getInstance().getStatement();

    public static boolean getProductById(int id){
        try {
            ResultSet resultSet = statement.executeQuery("SELECT *  from listaproduse where id ='"+id+"'");
            if(resultSet != null )
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void insertProduct(int id, String userId, int productId, int amount, int checked){
        if(!getProductById(id)){
            System.out.println("ID already exist in the shopping lists");
        }
        else {
            try {
                statement.executeUpdate("INSERT INTO listaproduse(id, user_id, produs_id, cantitate, checked )" +
                        " VALUE ('" + id + "','" + userId + "','" + productId +"','" + amount +  "','" + checked + "')");
                System.out.println("Shopping list inserted from database");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    public static String getCategoriesString()
    {
        String shList = "Result Set: ";
        try {
            ResultSet rs = statement.executeQuery("select * from listaproduse");
            while(rs.next()) {
                shList += rs.getString(1) + " " + rs.getString(2)+" "+rs.getString(3);
            }
            return shList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return shList;

    }
    public static ResultSet selectById(String userId){
        try {
            ResultSet resultSet = statement.executeQuery("SELECT *  from listaproduse where user_id ='"+userId+"'");
            if(resultSet != null )
                return resultSet;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ResultSet slectAll(){
        try {
            ResultSet resultSet = statement.executeQuery("SELECT *  from listaproduse ");
            if(resultSet != null )
                return resultSet;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static void deleteById(int id){
        try {
            statement.executeUpdate("delete from listaproduse where id ='"+id+"'");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateProduct(int id, String userId, int productId, int amount, int checked){
            try {
                statement.executeUpdate("UPDATE listaproduse SET cantitate ='" 	+ amount  +

                        "'WHERE id='"	+ id +"'");

                System.out.println("Shopping list inserted from database");
            } catch (SQLException e) {
                e.printStackTrace();
            }

    }

    public static JSONObject getJson(ResultSet rs){
        JSONObject jsonObject = new JSONObject();
        try {
            while(rs.next()) {
                //creating a map with all elements in the database
                Map<String,Object> map = new HashMap();

                map.put("id", rs.getInt(1));
                map.put("user_id", rs.getString(2));
                map.put("product_id", rs.getInt(3));
                map.put("amount", rs.getString(4));
                map.put("checked", rs.getInt(5));

                //creating the json object
                jsonObject.append("data", new JSONObject(map));
            }
            return jsonObject;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jsonObject;

    }
}
